# Dinamik Re-Unite 2018 Website

## Development
This project use npm package. Make sure `node.js` and `npm` are installed.

### Installing Dependencies

```
npm install
```

### Compiling assets
This project use webpack and laravel-mix to manage assets.

To compile:

```
npm run dev
```

To use auto-reloading with BrowserSync, place this project in your webroot
directory and change `proxy` directive in `webpack.mix.js` (e.g.:
`proxy: 'localhost/dinamik.cs.upi.edu'`). Then run:

```
npm run watch
```
